#ifndef CORRELATION_H
#define CORRELATION_H
#include "opencv2/core/core.hpp"

class Correlation
{
public:
    Correlation();
    static cv::Point2d phaseCorrelation2d(cv::Mat src1, cv::Mat src2);
private:
    static void magSpectr(cv::Mat src, cv::OutputArray _dst);
    static void divSpectr(cv::Mat srcA, cv::Mat srcB, cv::OutputArray _dst);
};

#endif // CORRELATION_H
