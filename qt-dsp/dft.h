#ifndef DFT_DFT_H
#define DFT_DFT_H

#include <cmath>
#include <vector>
#include <complex>
#include <valarray>
#include <iostream>


class dft
{
public:
    static std::vector<std::complex<double>>
                                          computeDFT(const std::vector<std::complex<double>> &input, bool inverse = false)
    {
        std::vector<std::complex<double>> output;
        auto n = input.size();

        for (size_t k = 0; k < n; k++)
        {
            std::complex<double> sum(0.0, 0.0);
            for (size_t t = 0; t < n; t++)
            {
                double integers = inverse ? -2. * t * k : 2. * t * k;

                std::complex<double> exponent(0.0, M_PI / n * integers);
                sum += input[t] * std::exp(exponent);
            }
            output.emplace_back(inverse ? sum / static_cast<double>(n) : sum);
        }
        return output;
    }

    static std::vector<std::complex<double>>
                                          computeMatrixDFT(const std::vector<std::complex<double>> &input, bool inverse = false)
    {
        auto n = input.size();
        auto ftMatrix = getFTMatrix(n, inverse);

        std::vector<std::complex<double>> output(n);

        for (size_t k = 0; k < n; k++)
        {
            std::complex<double> sum(0.0, 0.0);
            for (size_t t = 0; t < n; t++)
            {
                output[k] += ftMatrix[k][t] * (inverse ? input[t] / static_cast<double>(n) : input[t]);
            }
        }

        return output;
    }

private:
    static std::vector<std::vector<std::complex<double>>> getFTMatrix(ulong n, bool inverse = false)
    {
        std::vector<std::vector<std::complex<double>>> output(n);

        for (size_t k = 0; k < n; k++)
        {
            for (size_t t = 0; t < n; t++)
            {
                std::complex<double> i(0.0, inverse ? -1.0 : 1.0);
                output[k].emplace_back(std::exp(i * (((2 * M_PI) / static_cast<double>(n) * (t * k)))));
            }
        }
        return output;
    }
};


#endif
