#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qcustomplot.h"
#include <opencv2/core/core.hpp>

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void checkMatrixDft();

    void checkLinearity();
    void checkParseval();
    void analyzeWAV();
    void findPhaseShift();
    void checkSNRNormal();
    void checkSNRUniform();
    void luminance();
private:
    Ui::MainWindow *ui;
    QVector<QCPTextElement *> addedElements;
    void clearAllGraphs();
    void connectToGraphs();
    void rescaleGraphs();
    void enableGraphsInteraction();
    void disableGraphs();
    void checkTimeShift();
    double quantazing(QVector<double> &X, int a, int M, int R, bool normalTheoretical = false);
    double SNR(QVector<double> &X, QVector<double> &Y);
    void checkSNR(bool isNormal = false);
    double integral(std::function<double(double)> f, std::function<double(double)> g, double a, double b, int n)
    {
        double step = (b - a) / n; // width of rectangle
        double area = 0.0;
        double y = 0;  // height of rectangle

        for (int i = 0; i < n; ++i)
        {
            y = f(a + (i + 0.5) * step) * g(a + (i + 0.5) * step);
            area += y * step; // find the area of the rectangle and add it to the previous area. Effectively summing up the area under the curve.
        }

        return area;
    }
    void uniformQuantizing(cv::Mat &in, cv::Mat &out, int a, int R);
    double snrMat(cv::Mat &X, cv::Mat &Y);
    double PSNR(const cv::Mat &I, const cv::Mat &K);
    cv::Mat maxLloyd(const cv::Mat &in, int N);
    double calculateEntropy(const cv::Mat &in);
};

#endif // MAINWINDOW_H
