#include "correlation.h"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

Correlation::Correlation()
{

}

cv::Point2d Correlation::phaseCorrelation2d(cv::Mat src1, cv::Mat src2)
{
    int M = cv::getOptimalDFTSize(src1.rows);
    int N = cv::getOptimalDFTSize(src1.cols);

    cv::Mat padded1, padded2;

    if (M != src1.rows || N != src1.cols)
    {
        copyMakeBorder(src1, padded1, 0, M - src1.rows, 0, N - src1.cols, cv::BORDER_CONSTANT, cv::Scalar::all(0));
        copyMakeBorder(src2, padded2, 0, M - src2.rows, 0, N - src2.cols, cv::BORDER_CONSTANT, cv::Scalar::all(0));
    }
    else
    {
        padded1 = src1;
        padded2 = src2;
    }

    cv::Mat FFT1, FFT2, P, Pm, C;

    cv::dft(padded1, FFT1, cv::DFT_REAL_OUTPUT);
    cv::dft(padded2, FFT2, cv::DFT_REAL_OUTPUT);

    cv::mulSpectrums(FFT1, FFT2, P, 0, true);
    magSpectr(P, Pm);
    divSpectr(P, Pm, C); // FF* / |FF*| (phase correlation equation completed here...)
    idft(C, C);
    cv::imshow("Image", C);

    Point peakLoc;
    minMaxLoc(C, nullptr, nullptr, nullptr, &peakLoc);

    Point2d result;
    result.x = (peakLoc.x < src1.cols / 2) ? (peakLoc.x) : (peakLoc.x - src1.cols);
    result.y = (peakLoc.y < src1.rows / 2) ? (peakLoc.y) : (peakLoc.y - src1.rows);

    return peakLoc;
}

void Correlation::magSpectr(cv::Mat src, OutputArray _dst)
{
    int rows = src.rows, cols = src.cols;
    _dst.create(src.rows, src.cols, src.type());

    Mat dst = _dst.getMat();
    dst.setTo(0);

    int ncols = cols;
    int j0 = 1;
    int j1 = ncols - (cols % 2 == 0 );

    const float *dataSrc = src.ptr<float>();
    float *dataDst = dst.ptr<float>();

    size_t stepSrc = src.step / sizeof(dataSrc[0]);
    size_t stepDst = dst.step / sizeof(dataDst[0]);

    for (int k = 0; k < (cols % 2 ? 1 : 2); k++)
    {
        if (k == 1)
        {
            dataSrc += cols - 1;
            dataDst += cols - 1;
        }
        dataDst[0] = dataSrc[0] * dataSrc[0];

        if (rows % 2 == 0)
        {
            dataDst[(rows - 1)*stepDst] = dataSrc[(rows - 1) * stepSrc] * dataSrc[(rows - 1) * stepSrc];
        }

        for (int j = 1; j <= rows - 2; j += 2)
        {
            dataDst[j * stepDst] = (float)std::sqrt((double)dataSrc[j * stepSrc] * dataSrc[j * stepSrc] +
                                                    (double)dataSrc[(j + 1) * stepSrc] * dataSrc[(j + 1) * stepSrc]);
        }

        if (k == 1)
        {
            dataSrc -= cols - 1;
            dataDst -= cols - 1;
        }
    }

    for (; rows--; dataSrc += stepSrc, dataDst += stepDst)
    {
        for (int j = j0; j < j1; j += 2 )
        {
            dataDst[j] = (float)std::sqrt((double)dataSrc[j] * dataSrc[j] + (double)dataSrc[j + 1] * dataSrc[j + 1]);
        }
    }
}

void Correlation::divSpectr(cv::Mat srcA, cv::Mat srcB, OutputArray _dst)
{
    int rows = srcA.rows, cols = srcA.cols;

    _dst.create(srcA.rows, srcA.cols, srcA.type());
    Mat dst = _dst.getMat();

    int ncols = cols ;
    int j0 = 1;
    int j1 = ncols - (cols % 2 == 0 );

    const float *dataA = srcA.ptr<float>();
    const float *dataB = srcB.ptr<float>();
    float *dataC = dst.ptr<float>();
    float eps = FLT_EPSILON;

    size_t stepA = srcA.step / sizeof(dataA[0]);
    size_t stepB = srcB.step / sizeof(dataB[0]);
    size_t stepC = dst.step / sizeof(dataC[0]);

    for (int k = 0; k < (cols % 2 ? 1 : 2); k++)
    {
        if (k == 1)
        {
            dataA += cols - 1;
            dataB += cols - 1;
            dataC += cols - 1;
        }

        dataC[0] = dataA[0] / (dataB[0] + eps);

        if (rows % 2 == 0)
        {
            dataC[(rows - 1) * stepC] = dataA[(rows - 1) * stepA] / (dataB[(rows - 1) * stepB] + eps);
        }

        for (int j = 1; j <= rows - 2; j += 2 )
        {
            double denom = (double)dataB[j * stepB] * dataB[j * stepB] +
                           (double)dataB[(j + 1) * stepB] * dataB[(j + 1) * stepB] + (double)eps;

            double re = (double)dataA[j * stepA] * dataB[j * stepB] +
                        (double)dataA[(j + 1) * stepA] * dataB[(j + 1) * stepB];

            double im = (double)dataA[(j + 1) * stepA] * dataB[j * stepB] -
                        (double)dataA[j * stepA] * dataB[(j + 1) * stepB];

            dataC[j * stepC] = (float)(re / denom);
            dataC[(j + 1)*stepC] = (float)(im / denom);
        }

        if (k == 1)
        {
            dataA -= cols - 1;
            dataB -= cols - 1;
            dataC -= cols - 1;
        }
    }

    for (; rows--; dataA += stepA, dataB += stepB, dataC += stepC)
    {
        for (int j = j0; j < j1; j += 2)
        {
            double denom = (double)(dataB[j] * dataB[j] + dataB[j + 1] * dataB[j + 1] + eps);
            double re = (double)(dataA[j] * dataB[j] + dataA[j + 1] * dataB[j + 1]);
            double im = (double)(dataA[j + 1] * dataB[j] - dataA[j] * dataB[j + 1]);
            dataC[j] = (float)(re / denom);
            dataC[j + 1] = (float)(im / denom);
        }
    }

}
