#-------------------------------------------------
#
# Project created by QtCreator 2017-10-29T15:07:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport multimedia core

TARGET = qt-dsp
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

LIBS += -L/usr/lib -lsndfile -lfftw3 -lopencv_core -lopencv_imgcodecs -lopencv_highgui -lopencv_imgproc -lopencv_plot

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    qcustomplot.cpp \
    wavreader.cpp \
    correlation.cpp

HEADERS += \
        mainwindow.h \
    qcustomplot.h \
    dft.h \
    wavreader.h \
    correlation.h

FORMS += \
        mainwindow.ui

DISTFILES += \
    waveFiles/1.wav \
    waveFiles/2.wav \
    waveFiles/3.wav \
    waveFiles/4.wav \
    waveFiles/5.wav \
    waveFiles/6.wav \
    waveFiles/7.wav \
    waveFiles/8.wav \
    waveFiles/9.wav \
    waveFiles/10.wav \
    waveFiles/11.wav \
    waveFiles/12.wav \
    waveFiles/13.wav \
    waveFiles/14.wav \
    waveFiles/15.wav \
    waveFiles/16.wav \
    waveFiles/17.wav \
    waveFiles/18.wav \
    waveFiles/19.wav \
    waveFiles/20.wav
