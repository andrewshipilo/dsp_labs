#include "wavreader.h"
#include <sndfile.hh>

#define	BUFFER_LEN 16000

WavReader::WavReader()
{

}

QVector<double> WavReader::readWavDouble(std::string fileName)
{
    QVector<double> result(BUFFER_LEN);
    const char *fname = fileName.c_str();

    static double buffer [BUFFER_LEN] ;

    SndfileHandle file ;

    file = SndfileHandle (fname) ;

    printf ("    Opened file '%s'\n", fname) ;
    printf ("    Sample rate : %d\n", file.samplerate ()) ;
    printf ("    Channels    : %d\n", file.channels ()) ;

    file.read(buffer, BUFFER_LEN) ;
    puts ("") ;

    for (int i = 0; i < BUFFER_LEN; i++)
    {
        result[i] = buffer[i];
    }

    return result;
}
