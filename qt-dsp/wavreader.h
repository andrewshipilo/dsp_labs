#ifndef WAVREADER_H
#define WAVREADER_H
#include <QCoreApplication>
#include <QtEndian>
#include <QDebug>
#include <QFile>
#include <QDataStream>

class WavReader
{
    //Wav Header
    class WavHeader
    {
    public:
        char chunkId[4]; //"RIFF" = 0x46464952
        quint32 chunkSize; //28 [+ sizeof(wExtraFormatBytes) + wExtraFormatBytes] + sum(sizeof(chunk.id) + sizeof(chunk.size) + chunk.size)
        char format[4]; //"WAVE" = 0x45564157
        char subchunk1ID[4]; //"fmt " = 0x20746D66
        quint32 subchunk1Size; //16 [+ sizeof(wExtraFormatBytes) + wExtraFormatBytes]
        quint16 audioFormat;
        quint16 numChannels;
        quint32 sampleRate;
        quint32 byteRate;
        quint16 blockAlign;
        quint16 bitsPerSample;
        //[quint16 wExtraFormatBytes;]
        //[Extra format bytes]
    };
public:
    WavReader();
    static QVector<double> readWavDouble(std::string fileName);
private:
    static void createFile(const char *fname, int format);
    static void readFile(const char *fname);
};

#endif // WAVREADER_H
