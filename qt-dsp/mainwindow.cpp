#include "dft.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSound>
#include "wavreader.h"
#include "correlation.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/plot.hpp>
#include <random>
#define X_SIZE 150


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    MainWindow::disableGraphs();

    connect(ui->actionMatrix_DFT_check, &QAction::triggered,
            this, &MainWindow::checkMatrixDft);

    connect(ui->actionCheck_Linearity, &QAction::triggered,
            this, &MainWindow::checkLinearity);

    connect(ui->actionCheck_Parseval_s_theorem, &QAction::triggered,
            this, &MainWindow::checkParseval);

    connect(ui->actionCheck_TimeShift, &QAction::triggered,
            this, &MainWindow::checkTimeShift);

    connect(ui->actionAnalyze_WAV, &QAction::triggered,
            this, &MainWindow::analyzeWAV);

    connect(ui->actionPhase, &QAction::triggered,
            this, &MainWindow::findPhaseShift);

    connect(ui->actionUniform, &QAction::triggered,
            this, &MainWindow::checkSNRUniform);

    connect(ui->actionNormal, &QAction::triggered,
            this, &MainWindow::checkSNRNormal);

    connect(ui->actionLuminance, &QAction::triggered,
            this, &MainWindow::luminance);
}

MainWindow::~MainWindow()
{
    MainWindow::disableGraphs();
    MainWindow::clearAllGraphs();
    delete ui;
}

void MainWindow::clearAllGraphs()
{
    ui->plot1->clearGraphs();
    ui->plot2->clearGraphs();
    ui->plot3->clearGraphs();

    ui->plot1->clearItems();
    ui->plot2->clearItems();
    ui->plot3->clearItems();

    ui->plot1->clearPlottables();
    ui->plot2->clearPlottables();
    ui->plot3->clearPlottables();

    for (auto element : addedElements)
    {
        delete element;
    }
    addedElements.clear();

    ui->plot1->plotLayout()->simplify();
    ui->plot2->plotLayout()->simplify();
    ui->plot3->plotLayout()->simplify();
}

void MainWindow::disableGraphs()
{
    ui->centralWidget->setEnabled(false);
    ui->plot1->setEnabled(false);
    ui->plot2->setEnabled(false);
    ui->plot3->setEnabled(false);

    ui->plot1->hide();
    ui->plot2->hide();
    ui->plot3->hide();
    ui->label->hide();
    ui->label_2->hide();
}

void MainWindow::connectToGraphs()
{
    // make left and bottom axes always transfer their ranges to right and top axes:
    connect(ui->plot1->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->plot1->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->plot1->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->plot1->yAxis2, SLOT(setRange(QCPRange)));

    connect(ui->plot2->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->plot2->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->plot2->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->plot2->yAxis2, SLOT(setRange(QCPRange)));

    connect(ui->plot3->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->plot3->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->plot3->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->plot3->yAxis2, SLOT(setRange(QCPRange)));
}

void MainWindow::rescaleGraphs()
{
    // let the ranges scale themselves so graph 0 fits perfectly in the visible area:
    ui->plot1->graph(0)->rescaleAxes();
    ui->plot2->graph(0)->rescaleAxes();
    ui->plot3->graph(0)->rescaleAxes();
}

void MainWindow::enableGraphsInteraction()
{
    // Note: we could have also just called ui->customPlot->rescaleAxes(); instead
    // Allow user to drag axis ranges with mouse, zoom with mouse wheel and select graphs by clicking:
    ui->plot1->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    ui->plot2->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    ui->plot3->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
}

void MainWindow::checkMatrixDft()
{
    MainWindow::disableGraphs();
    MainWindow::clearAllGraphs();

    // add two new graphs and set their look:
    QCPTextElement *title1 = new QCPTextElement(ui->plot1, "Source function", QFont("sans", 10));

    ui->plot1->addGraph();
    ui->plot1->graph(0)->setPen(QPen(Qt::blue)); // line color blue for first graph
    ui->plot1->graph(0)->setBrush(QBrush(QColor(0, 0, 255, 20))); // first graph will be filled with translucent blue
    ui->plot1->plotLayout()->insertRow(0);
    ui->plot1->plotLayout()->addElement(0, 0, title1);
    addedElements.push_back(title1);

    QCPTextElement *title2 = new QCPTextElement(ui->plot2, "Restored function", QFont("sans", 10));

    ui->plot2->addGraph();
    ui->plot2->graph(0)->setPen(QPen(Qt::red));
    ui->plot2->plotLayout()->insertRow(0);
    ui->plot2->plotLayout()->addElement(0, 0, title2);
    addedElements.push_back(title2);

    QCPTextElement *title3 = new QCPTextElement(ui->plot3, "Spectrum", QFont("sans", 10));

    ui->plot3->addGraph();
    ui->plot3->graph(0)->setPen(QPen(Qt::black));
    ui->plot3->graph(0)->setLineStyle(QCPGraph::LineStyle::lsImpulse);
    ui->plot3->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2));
    ui->plot3->plotLayout()->insertRow(0);
    ui->plot3->plotLayout()->addElement(0, 0, title3);
    addedElements.push_back(title3);

    QVector<double> x(X_SIZE), y(X_SIZE), y2(X_SIZE), dftRes(X_SIZE);
    for (int i = 0; i < x.size(); ++i)
    {
        x[i] = i;
        y[i] = qExp(-i / 100.0) * qCos(i / 7.); // exponentially decaying cosine
    }

    std::vector<std::complex<double>> input;
    for (auto point : y)
    {
        input.emplace_back(point, 0.0);
    }

    auto output = dft::computeMatrixDFT(input);
    auto inverseOutput = dft::computeMatrixDFT(output, true);

    QTextStream out(stdout);
    out << "Input: " << endl;
    for (auto num : input)
    {
        out << "( " << num.real() << " : " << num.imag() << " ) " << endl;
    }
    out << endl;

    out << "DFT: " << endl;
    for (size_t i = 0; i < output.size(); ++i)
    {
        dftRes[static_cast<int>(i)] = abs(output[i]);
    }
    out << endl;

    out << "Inverse DFT: " << " elements " << inverseOutput.size() <<  endl;
    for (size_t i = 0; i < inverseOutput.size(); ++i)
    {
        y2[static_cast<int>(i)] = inverseOutput[i].real();
    }
    out << endl;

    // pass data points to graphs:
    ui->plot1->graph(0)->setData(x, y);
    ui->plot2->graph(0)->setData(x, y2);
    ui->plot3->graph(0)->setData(x, dftRes);

    MainWindow::connectToGraphs();
    MainWindow::rescaleGraphs();
    MainWindow::enableGraphsInteraction();


    ui->centralWidget->setEnabled(true);
    ui->plot1->setEnabled(true);
    ui->plot2->setEnabled(true);
    ui->plot3->setEnabled(true);

    ui->plot1->show();
    ui->plot2->show();
    ui->plot3->show();
}

void MainWindow::checkLinearity()
{
    MainWindow::disableGraphs();
    MainWindow::clearAllGraphs();

    QTextStream out(stdout);

    QCPTextElement *title1 = new QCPTextElement(ui->plot1, "Spectrum au(k) + bu(k) after DFT", QFont("sans", 10));

    ui->plot1->addGraph();
    ui->plot1->graph(0)->setPen(QPen(Qt::black));
    ui->plot1->graph(0)->setLineStyle(QCPGraph::LineStyle::lsImpulse);
    ui->plot1->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2));
    ui->plot1->plotLayout()->insertRow(0);
    ui->plot1->plotLayout()->addElement(0, 0, title1);
    addedElements.push_back(title1);

    QCPTextElement *title2 = new QCPTextElement(ui->plot2, "Spectrum aU(n) + bU(n)", QFont("sans", 10));

    ui->plot2->addGraph();
    ui->plot2->graph(0)->setPen(QPen(Qt::black));
    ui->plot2->graph(0)->setLineStyle(QCPGraph::LineStyle::lsImpulse);
    ui->plot2->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2));
    ui->plot2->plotLayout()->insertRow(0);
    ui->plot2->plotLayout()->addElement(0, 0, title2);
    addedElements.push_back(title2);

    ui->plot3->addGraph();


    QVector<double> x(X_SIZE), y(X_SIZE), spectrumOne(X_SIZE), spectrumTwo(X_SIZE);

    for (int i = 0; i < x.size(); ++i)
    {
        x[i] = i;
        y[i] = qExp(-i / 100.0) * qCos(i / 7.); // exponentially decaying cosine
    }

    std::vector<std::complex<double>> input;
    for (auto point : y)
    {
        input.emplace_back(point, 0.0);
    }

    auto output = dft::computeDFT(input);

    out << "Linearity: au(k) + bu(k) <-> aU(n) + bU(n) \n\n";
    double alpha = 3.5;
    double beta = 0.4;

    std::vector<std::complex<double>> u1 = input, u2 = input;
    std::vector<std::complex<double>> left, right;
    std::vector<std::complex<double>> U1 = output, U2 = output;
    for (size_t i = 0; i < input.size(); i++)
    {
        u1[i] *= alpha;
        u2[i] *= beta;
        left.emplace_back(u1[i] + u2[i]);

        U1[i] *= alpha;
        U2[i] *= beta;
        right.emplace_back(U1[i] + U2[i]);
    }

    auto linear_output = dft::computeDFT(left);

    for (size_t i = 0; i < right.size(); ++i)
    {
        spectrumOne[static_cast<int>(i)] = right[i].real();
    }

    for (size_t i = 0; i < linear_output.size(); ++i)
    {
        spectrumTwo[static_cast<int>(i)] = linear_output[i].real();
    }


    ui->plot1->graph(0)->setData(x, spectrumOne);
    ui->plot2->graph(0)->setData(x, spectrumTwo);

    MainWindow::connectToGraphs();
    MainWindow::rescaleGraphs();
    MainWindow::enableGraphsInteraction();

    ui->centralWidget->setEnabled(true);
    ui->plot1->setEnabled(true);
    ui->plot2->setEnabled(true);

    ui->plot1->show();
    ui->plot2->show();
}

void MainWindow::checkParseval()
{
    MainWindow::disableGraphs();
    MainWindow::clearAllGraphs();
    qInfo() << "Parseval's theorem: sum(u^2(k)) == 1/N * sum(abs(U^2(n)))";
    QString labelOneText = "Time domain: ";
    QString labelTwoText = "Frequency domain: ";

    QVector<double> y(X_SIZE);

    for (int i = 0; i < y.size(); ++i)
    {
        y[i] = qExp(-i / 100.0) * qCos(i / 7.); // exponentially decaying cosine
    }

    std::vector<std::complex<double>> input;

    for (auto point : y)
    {
        input.emplace_back(point, 0.0);
    }
    auto output = dft::computeDFT(input);

    double time_energy = 0.0;
    double frequency_energy = 0.0;

    for (auto num : input)
    {
        time_energy += abs(num * num);
    }

    for (auto num : output)
    {
        frequency_energy += abs(num * num);
    }
    frequency_energy /= output.size();

    labelOneText.append("<span style=\"color:orange;\">" + QString::number(time_energy) + "</span>; ").append(labelTwoText)
    .append("<span style=\"color:orange;\">" + QString::number(frequency_energy) + "</span>");


    ui->label->setText(labelOneText);
    ui->label->show();

    ui->centralWidget->setEnabled(true);
}

void MainWindow::checkTimeShift()
{
    MainWindow::disableGraphs();
    MainWindow::clearAllGraphs();

    QCPTextElement *title1 = new QCPTextElement(ui->plot1, "Shifted function", QFont("sans", 10));

    ui->plot1->addGraph();
    ui->plot1->graph(0)->setPen(QPen(Qt::blue)); // line color blue for first graph
    ui->plot1->graph(0)->setBrush(QBrush(QColor(0, 0, 255, 20))); // first graph will be filled with translucent blue
    ui->plot1->plotLayout()->insertRow(0);
    ui->plot1->plotLayout()->addElement(0, 0, title1);
    addedElements.push_back(title1);

    QCPTextElement *title2 = new QCPTextElement(ui->plot2, "Spectrum of source func * e^(...)", QFont("sans", 10));

    ui->plot2->addGraph();
    ui->plot2->graph(0)->setPen(QPen(Qt::black));
    ui->plot2->graph(0)->setLineStyle(QCPGraph::LineStyle::lsImpulse);
    ui->plot2->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2));
    ui->plot2->plotLayout()->insertRow(0);
    ui->plot2->plotLayout()->addElement(0, 0, title2);
    addedElements.push_back(title2);

    QCPTextElement *title3 = new QCPTextElement(ui->plot3, "Spectrum of shifted func", QFont("sans", 10));

    ui->plot3->addGraph();
    ui->plot3->graph(0)->setPen(QPen(Qt::black));
    ui->plot3->graph(0)->setLineStyle(QCPGraph::LineStyle::lsImpulse);
    ui->plot3->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2));
    ui->plot3->plotLayout()->insertRow(0);
    ui->plot3->plotLayout()->addElement(0, 0, title3);
    addedElements.push_back(title3);


    QVector<double> x(X_SIZE), y(X_SIZE), yShifted(X_SIZE), spectrumSource(X_SIZE), spectrumShifted(X_SIZE);

    for (int i = 0; i < x.size(); ++i)
    {
        x[i] = i;
        y[i] = qCos(i / 7.); // exponentially decaying cosine
    }

    int shift = 25;
    for (int i = 0; i < x.size(); ++i)
    {
        yShifted[i] = qCos((i - shift) / 7.); // exponentially decaying cosine with shift of 25
    }

    std::vector<std::complex<double>> input;
    for (auto point : y)
    {
        input.emplace_back(point, 0.0);
    }

    std::vector<std::complex<double>> inputShifted;
    for (auto point : yShifted)
    {
        inputShifted.emplace_back(point, 0.0);
    }

    auto output = dft::computeDFT(input);
    auto outputShifted = dft::computeDFT(inputShifted);

    for (size_t i = 0; i < output.size(); ++i)
    {
        std::complex<double> j(0.0, -1.0);

        output[i] *= std::exp(j * (((2 * M_PI * -i * shift) / static_cast<double>(output.size()))));

        spectrumSource[static_cast<int>(i)] = abs(output[i]);
    }


    for (size_t i = 0; i < outputShifted.size(); ++i)
    {
        spectrumShifted[static_cast<int>(i)] = abs(outputShifted[i]);
    }

    ui->plot1->graph(0)->setData(x, yShifted);
    ui->plot2->graph(0)->setData(x, spectrumSource);
    ui->plot3->graph(0)->setData(x, spectrumShifted);

    MainWindow::connectToGraphs();
    MainWindow::rescaleGraphs();
    MainWindow::enableGraphsInteraction();

    ui->centralWidget->setEnabled(true);
    ui->plot1->setEnabled(true);
    ui->plot2->setEnabled(true);
    ui->plot3->setEnabled(true);

    ui->plot1->show();
    ui->plot2->show();
    ui->plot3->show();
}
template<typename T>
int sgn(T val)
{
    return (T(0) < val) - (val < T(0));
}

double MainWindow::quantazing(QVector<double> &X, int a, int M, int R, bool normalTheoretical/* = false*/)
{
    /* 1.2 применить к выборке {x1, ..., xM} равномерное квантование с числом квантов 2^R, где R = 1, ..., 7. */
    auto N = static_cast<int>(std::pow(2, R));
    double delta = a / (N / 2.);
    QVector<double> Y(N);
    double current = -a;

    for (auto i = 1; i <= N; i++)
    {
        double next = current + delta;
        Y[i - 1] = (current + next) / 2.;
        current = next;
    }

    QVector<double> quantizedX(M);
    for (int i = 0; i < M; i++)
    {
        if (std::abs(X[i]) > Y.back())
        {
            quantizedX[i] = sgn(X[i]) * Y.back();
            continue;
        }
        for (int j = N / 2; j <= N; j++)
        {
            if (std::abs(X[i]) > std::abs(Y[j]))
            {
                continue;
            }
            else
            {
                double distLeft = std::abs(std::abs(Y[j - 1]) - std::abs(X[i]));
                double distRight = std::abs(std::abs(Y[j]) - std::abs(X[i]));
                quantizedX[i] = sgn(X[i]) * (distLeft < distRight ? Y[j - 1] : Y[j]);
                break;
            }
        }
    };
    QVector<double> x_axis;
    for (int i = 0; i < M; i++)
    {
        x_axis.push_back(i);
    }

    if (R == 5)
    {
        QVector<double> ALL;
        for (int i = 0; i < M; i++)
        {
            ALL.push_back(i);
        }
        //ui->plot3->graph(0)->setData(X, quantizedX);
        ui->plot3->graph(0)->setData(ALL, quantizedX);
    }

    /*1.3 По результатам эксперимента построить график SNR(R), воспользовавшись формулой (3.4)
     * и сравнить его с теоретическим, полученным по формуле (3.11).
     * Для вычисления SNR по теоретическим значениям, используя дисперсию исследуемого закона распределения,
     * следует пользоваться формулой (3.5).*/

    qInfo() << SNR(X, quantizedX);
    qInfo() << (6.02 * R);

    if (normalTheoretical) // Формула 3.6
    {
        double sum = 0;
        double sigma2 = static_cast<double>(std::pow((a / 3.f), 2));
        for (int i = 1; i < quantizedX.size(); i++)
        {
            sum += MainWindow::integral([quantizedX, i] (double x) -> double
            {
                return std::pow(x - quantizedX[i], 2);
            },
            [sigma2] (double x) -> double
            {
                return ((1 / std::sqrt(2 * M_PI * sigma2)) * std::exp(-(std::pow(x, 2) / (2 * sigma2))));
            },
            X[i],
            X[i - 1],
            100);
        }
        double result = -std::log10(sigma2 / std::abs(sum));

        qInfo() << "3.6: " << result;
        return result;
    }
    else
        return SNR(X, quantizedX);
}

double MainWindow::SNR(QVector<double> &X, QVector<double> &Y)
{
    double numerator = 0;
    double denumerator = 0;
    double result = 0;

    for (int i = 0; i < X.size(); i++)
    {
        numerator += std::pow(X[i], 2);
        denumerator += std::pow((X[i] - Y[i]), 2);
    }
    result = 10 * std::log10(numerator / denumerator);
    return result;
}

double MainWindow::snrMat(cv::Mat &X, cv::Mat &Y)
{
    double numerator = 0;
    double denumerator = 0;
    double result = 0;

    for (int i = 0; i < X.rows; i++)
    {
        for (int j = 0; j < X.cols; j++)
        {
            numerator += std::pow(X.at<uchar>(i, j), 2);
            denumerator += std::pow((X.at<uchar>(i, j) - Y.at<uchar>(i, j)), 2);
        }
    }
    result = 10 * std::log10(numerator / denumerator);
    return result;
}

void MainWindow::analyzeWAV()
{
    MainWindow::disableGraphs();
    MainWindow::clearAllGraphs();
    QTextStream out(stdout);

    QCPTextElement *title1 = new QCPTextElement(ui->plot1, "WAVE function", QFont("sans", 10));

    ui->plot1->addGraph();
    ui->plot1->graph(0)->setPen(QPen(Qt::blue)); // line color blue for first graph
    ui->plot1->graph(0)->setLineStyle(QCPGraph::LineStyle::lsImpulse);
    ui->plot1->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2));
    ui->plot1->plotLayout()->insertRow(0);
    ui->plot1->plotLayout()->addElement(0, 0, title1);
    addedElements.push_back(title1);

    QCPTextElement *title2 = new QCPTextElement(ui->plot2, "Spectrum of WAVE function", QFont("sans", 10));

    ui->plot2->addGraph();
    ui->plot2->graph(0)->setPen(QPen(Qt::black));
    ui->plot2->graph(0)->setLineStyle(QCPGraph::LineStyle::lsImpulse);
    ui->plot2->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2));
    ui->plot2->plotLayout()->insertRow(0);
    ui->plot2->plotLayout()->addElement(0, 0, title2);
    addedElements.push_back(title2);

    ui->plot3->addGraph();


    QVector<QVector<QChar>> DTMFtable =
    {
        {'1', '2', '3', 'A'},
        {'4', '5', '6', 'B'},
        {'7', '8', '9', 'C'},
        {'*', '0', '#', 'D'}
    };

    QVector<int> f1 = {697, 770, 852, 941};
    QVector<int> f2 = {1209, 1336, 1477, 1633};
    int index1 = 0;
    int index2 = 0;


    QSound::play("/home/andrew/Projects/qt-dsp/waveFiles/13.wav");
    QVector<double> samples = WavReader::readWavDouble("/home/andrew/Projects/qt-dsp/waveFiles/13.wav");
    QVector<double> x(800), spectrumOne(800);
    std::vector<std::complex<double>> input(800);
    QString answer = "Decoded vector: ";

    for (int i = 0; i < x.size(); ++i)
    {
        x[i] = i;
    }

    int leftBorder = 0;
    for (int step = 0; step < 10; ++step)
    {
        for (int i = leftBorder; i < leftBorder + x.size(); ++i)
        {
            input[i - leftBorder].real(samples[i]);
        }

        leftBorder += 1600;
        auto output = dft::computeDFT(input);

        for (size_t i = 0; i < output.size(); ++i)
        {
            spectrumOne[static_cast<int>(i)] = abs(output[i]);
        }

        QVector<double> f(4);
        int counter = 0;
        for (int i = 0; i < spectrumOne.size(); ++i)
        {
            if (spectrumOne[i] > 110)
            {
                f[counter] = i * 10;
                counter++;
            }
            if (counter > 3)
                break;
        }

        double min = 500;
        for (int k = 0; k < 4; k++)
        {
            double tmp = abs(f[0] - f1[k]);
            if (min > tmp)
            {
                min = tmp;
                index1 = k;
            }
        }

        min = 500;
        for (int k = 0; k < 4; k++)
        {
            double tmp = abs(f[1] - f2[k]);
            if (min > tmp)
            {
                min = tmp;
                index2 = k;
            }
        }
        answer.append("<span style=\"color:orange;\">").append(DTMFtable[index1][index2]).append("</span> ");
    }

    ui->label_2->setText(answer);

    ui->plot1->graph(0)->setData(x, samples);
    ui->plot2->graph(0)->setData(x, spectrumOne);

    MainWindow::connectToGraphs();
    MainWindow::rescaleGraphs();
    MainWindow::enableGraphsInteraction();

    ui->centralWidget->setEnabled(true);
    ui->plot1->setEnabled(true);
    ui->plot2->setEnabled(true);

    ui->plot1->show();
    ui->plot2->show();
    ui->label_2->show();
}

void MainWindow::findPhaseShift()
{
    MainWindow::disableGraphs();
    MainWindow::clearAllGraphs();

    cv::Mat matFirst = cv::imread("/home/andrew/Downloads/pictures/tank_first.bmp", 0);
    cv::Mat matSecond = cv::imread("/home/andrew/Downloads/pictures/tank_second.bmp", 0);

    auto result = Correlation::phaseCorrelation2d(cv::Mat_<float>(matFirst), cv::Mat_<float>(matSecond));

    qInfo() << "Point is: (" << result.x << ":" << result.y << ")";
}

void MainWindow::checkSNRNormal()
{
    checkSNR(true);
}

void MainWindow::checkSNRUniform()
{
    checkSNR();
}

void MainWindow::checkSNR(bool isNormal/*= false*/)
{
    MainWindow::disableGraphs();
    MainWindow::clearAllGraphs();

    QCPTextElement *title1 = new QCPTextElement(ui->plot1, "SNR Experimental", QFont("sans", 10));

    ui->plot1->addGraph();
    ui->plot1->graph(0)->setPen(QPen(Qt::blue)); // line color blue for first graph
    ui->plot1->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
    ui->plot1->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2));
    ui->plot1->plotLayout()->insertRow(0);
    ui->plot1->plotLayout()->addElement(0, 0, title1);
    addedElements.push_back(title1);

    QCPTextElement *title2 = new QCPTextElement(ui->plot2, "SNR Theoretical", QFont("sans", 10));

    ui->plot2->addGraph();
    ui->plot2->graph(0)->setPen(QPen(Qt::black));
    ui->plot2->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
    ui->plot2->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2));
    ui->plot2->plotLayout()->insertRow(0);
    ui->plot2->plotLayout()->addElement(0, 0, title2);
    addedElements.push_back(title2);

    QCPTextElement *title3 = new QCPTextElement(ui->plot3, "Quantized values", QFont("sans", 10));

    ui->plot3->addGraph();
    ui->plot3->graph(0)->setPen(QPen(Qt::black));
    ui->plot3->graph(0)->setLineStyle(QCPGraph::LineStyle::lsStepRight);
    ui->plot3->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2));
    ui->plot3->plotLayout()->insertRow(0);
    ui->plot3->plotLayout()->addElement(0, 0, title3);
    addedElements.push_back(title3);

    int a = 24;
    int M = 10000;
    std::random_device rd;
    std::mt19937 gen(rd());
    QVector<double> X(M);

    if (isNormal)
    {
        std::normal_distribution<double> norm_dis(0, static_cast<double>(std::pow((a / 3.f), 2)));

        for (auto &entry : X)
        {
            entry = norm_dis(gen);
        }
    }
    else
    {
        std::uniform_real_distribution<double> uni_dis(-a, a);

        for (auto &entry : X)
        {
            entry = uni_dis(gen);
        }
    }

    qInfo() << X;
    QVector<double> experimentalSNR;
    QVector<double> theoreticalSNR;
    QVector<double> R;

    for (int i = 1; i <= 7; i++)
    {
        R.push_back(i);
        experimentalSNR.push_back(quantazing(X, a, M, i));
        if (!isNormal)
        {
            theoreticalSNR.push_back(6.02 * i);
        }
        else
        {
            theoreticalSNR.push_back(quantazing(X, a, M, i, true));
        }
    }

    ui->plot1->graph(0)->setData(R, experimentalSNR);
    ui->plot2->graph(0)->setData(R, theoreticalSNR);

    MainWindow::connectToGraphs();
    MainWindow::rescaleGraphs();
    MainWindow::enableGraphsInteraction();

    ui->centralWidget->setEnabled(true);
    ui->plot1->setEnabled(true);
    ui->plot2->setEnabled(true);
    ui->plot3->setEnabled(true);

    ui->plot1->show();
    ui->plot2->show();
    ui->plot3->show();
}

void MainWindow::uniformQuantizing(cv::Mat &in, cv::Mat &out, int a, int R)
{
    double N = static_cast<double>(std::pow(2, R));
    double delta = a / (N);
    QVector<uchar> Y(N);
    double current = 0;

    for (auto i = 1; i <= N; i++)
    {
        double next = current + delta;
        Y[i - 1] = (current + next) / 2.;
        current = next;
    }

    for (int i = 0; i < in.rows; i++)
    {
        for (int j = 0; j < in.cols; j++)
        {

            uchar val = in.at <uchar> (i, j);
            if (val > Y.back())
            {
                out.at<uchar>(i, j) = Y.back();
                continue;
            }

            for (int k = 1; k <= N; k++)
            {
                if (val > Y[k])
                {
                    continue;
                }
                else
                {
                    uchar distLeft = std::abs(std::abs(Y[k - 1]) - val);
                    uchar distRight = std::abs(std::abs(Y[k]) - val);
                    out.at<uchar>(i, j) = (distLeft < distRight ? Y[k - 1] : Y[k]);
                    break;
                }
            }
        }
    }
}

double MainWindow::PSNR(const cv::Mat &I, const cv::Mat &K)
{
    assert(I.rows == K.rows && I.cols == K.cols);

    /* K - is quantized image */
    int sum = 0;
    for (int i = 0; i < I.rows; i++)
    {
        for (int j = 0; j < I.cols; j++)
        {
            double minus = std::abs(I.at<uchar>(i, j) - K.at<uchar>(i, j));
            sum += std::pow(minus, 2);
        }
    }

    double MSE = sum / (double) (I.rows * I.cols);

    double PSNR = 10 * std::log10(65025 / MSE);

    return PSNR;
}

cv::Mat MainWindow::maxLloyd(const cv::Mat &in, int N)
{
    int k = 1;
    std::vector<double> t(N);
    std::vector<double> y;
    int Tk = N;
    cv::Point minLoc;
    cv::Point maxLoc;

    cv::minMaxLoc(in, nullptr, nullptr, &minLoc, &maxLoc);
    t[0] = in.at<uchar>(minLoc);
    t[N] = in.at<uchar>(maxLoc);

    qInfo() << t[0] << " : " << t[N];

    cv::Mat out = in.clone();
    for (; k < Tk; k++)
    {
        /* STEP 0 */
        t[k] = t[0] + k * ((t[N] - t[0]) / N);

        /* STEP 1 */
        int q = 0;
        double sum = 0;
        std::vector<uchar> x;
        std::vector<std::pair<int, int>> indxes;

        for (int i = 0; i < in.rows; i++)
        {
            for (int j = 0; j < in.cols; j++)
            {
                uchar val = in.at<uchar>(i, j);
                if (val > t[k - 1] && val < t[k])
                {
                    x.push_back(val);
                    sum += val;
                    q++;
                    indxes.push_back({i, j});
                }
            }
        }
        uchar y = sum / q;

        for (auto idx : indxes)
        {
            out.at<uchar>(idx.first, idx.second) = y;
        }

        /* STEP 2 */
        double eps = 0;
        sum = 0;
        for (int i = 0; i < in.rows; i++)
        {
            for (int j = 0; j < in.cols; j++)
            {
                sum += std::pow(in.at<uchar>(i, j) - out.at<uchar>(i, j), 2);
            }
        }
        eps = sum / (in.rows * in.cols);
    }

    //cv::imshow("MaxLloyd", out);
    return out;
}

double MainWindow::calculateEntropy(const cv::Mat &in)
{
    std::vector<double> probabilites(256);

    for (int i = 0; i < in.rows; i++)
    {
        for (int j = 0; j < in.cols - 1; j++)
        {
            uchar diff = in.at<uchar>(i, j + 1) - in.at<uchar>(i, j);
            probabilites[diff]++;
        }
    }

    double entropy = 0;
    for (size_t i = 0; i < probabilites.size(); i++)
    {
        probabilites[i] /= in.rows * in.cols;

        if (probabilites[i] != 0)
        {
            entropy -= probabilites[i] * std::log(probabilites[i]);
        }
    }

    entropy /= std::log(2);
    probabilites.clear();
    return entropy;
}

void MainWindow::luminance()
{
    MainWindow::disableGraphs();
    MainWindow::clearAllGraphs();

    cv::Mat darkImg = cv::imread("/home/andrew/Pictures/3rd_lab_images/dark.bmp", 1);
    cv::Mat lightImg =  cv::imread("/home/andrew/Pictures/3rd_lab_images/light.bmp", 1);

    cv::Mat yuvDark;

    cv::cvtColor(darkImg, yuvDark, CV_BGR2YUV);

    std::vector<cv::Mat> chanDark;

    cv::split(yuvDark, chanDark);

    cv::Mat yDark = chanDark[0];
    cv::imshow("Y DARK", yDark);

    int histSize = 512;

    float range[] = { 0, 256 };
    const float *histRange = { range };

    bool uniform = true;
    bool accumulate = false;

    cv::Mat dark_hist;
    cv::calcHist(&yDark, 1, 0, cv::Mat(), dark_hist, 1, &histSize, &histRange, uniform, accumulate);

    int histWidth = 512;
    int histHeight = 400;
    int binWidth = cvRound((double) histWidth / histHeight);

    cv::Mat histImg(histHeight, histWidth, CV_8UC3, cv::Scalar(0, 0, 0));

    cv::normalize(dark_hist, dark_hist, 0, histImg.rows, cv::NORM_MINMAX, -1, cv::Mat());

    for (int i = 1; i < histSize; i++)
    {
        line(histImg, cv::Point(binWidth * (i - 1), histHeight - cvRound(dark_hist.at<float>(i - 1))),
             cv::Point(binWidth * (i), histHeight - cvRound(dark_hist.at<float>(i))),
             cv::Scalar(255, 255, 255), 2, 8, 0);
    }
    cv::namedWindow("DarkImg Y histogram", CV_WINDOW_AUTOSIZE);
    cv::imshow("DarkImg Y histogram", histImg);

    cv::Mat quantizedYDark(cv::Size(yDark.cols, yDark.rows), CV_8UC1);

    std::vector<double> snrData;
    std::vector<double> psnrData;
    std::vector<double> entropyData;

    for (int i = 1; i <= 7; i++)
    {
        uniformQuantizing(yDark, quantizedYDark, 255, i);
        double snr = snrMat(yDark, quantizedYDark);
        snrData.push_back(snr);

        double psnr = PSNR(yDark, quantizedYDark);
        psnrData.push_back(psnr);

        entropyData.push_back(calculateEntropy(quantizedYDark));
    }



    cv::Mat snrPlotResult;
    cv::Ptr<cv::plot::Plot2d> snrPlot = cv::plot::Plot2d::create(snrData);
    snrPlot->setPlotBackgroundColor(cv::Scalar( 235, 235, 235 ));
    snrPlot->setPlotLineColor(cv::Scalar( 255, 50, 50 ));
    snrPlot->setInvertOrientation(true);
    snrPlot->setPlotGridColor(cv::Scalar( 35, 35, 35));
    snrPlot->setPlotTextColor(cv::Scalar( 35, 35, 35));
    snrPlot->render(snrPlotResult);
    cv::imshow("SNR(R)", snrPlotResult);

    cv::Mat psnrPlotResult;
    cv::Ptr<cv::plot::Plot2d> psnrPlot = cv::plot::Plot2d::create(psnrData);
    psnrPlot->setPlotBackgroundColor(cv::Scalar( 235, 235, 235 ));
    psnrPlot->setPlotLineColor(cv::Scalar( 50, 50, 255 ));
    psnrPlot->setInvertOrientation(true);
    psnrPlot->setPlotGridColor(cv::Scalar( 35, 35, 35));
    psnrPlot->setPlotTextColor(cv::Scalar( 35, 35, 35));
    psnrPlot->render(psnrPlotResult);
    cv::imshow("PSNR(R)", psnrPlotResult);

    cv::Mat entropyPlotResult;
    cv::Ptr<cv::plot::Plot2d> entropyPlot = cv::plot::Plot2d::create(entropyData, psnrData);
    entropyPlot->setPlotBackgroundColor(cv::Scalar( 235, 235, 235 ));
    entropyPlot->setPlotLineColor(cv::Scalar( 50, 50, 255 ));
    entropyPlot->setInvertOrientation(true);
    entropyPlot->setPlotGridColor(cv::Scalar( 35, 35, 35));
    entropyPlot->setPlotTextColor(cv::Scalar( 35, 35, 35));
    entropyPlot->render(entropyPlotResult);
    cv::imshow("PSNR(H)", entropyPlotResult);

    std::vector<double> lloydSnrData;
    std::vector<double> lloydPsnrData;
    std::vector<double> lloydEntropyData;

    cv::Mat quant;
    for (int i = 1; i <= 7; i++)
    {
        cv::Mat tmpQuant = maxLloyd(yDark, std::pow(2, i));
        double snr = snrMat(yDark, tmpQuant);
        lloydSnrData.push_back(snr);

        if (i == 4)
        {
            quant = tmpQuant.clone();
        }

        double psnr = PSNR(yDark, tmpQuant);
        lloydPsnrData.push_back(psnr);

        lloydEntropyData.push_back(calculateEntropy(tmpQuant));
    }

    cv::Mat lloydSnrPlotResult;
    cv::Ptr<cv::plot::Plot2d> lloydSnrPlot = cv::plot::Plot2d::create(lloydSnrData);
    lloydSnrPlot->setPlotBackgroundColor(cv::Scalar( 235, 235, 235 ));
    lloydSnrPlot->setPlotLineColor(cv::Scalar( 255, 50, 50 ));
    lloydSnrPlot->setInvertOrientation(true);
    lloydSnrPlot->setPlotGridColor(cv::Scalar( 35, 35, 35));
    lloydSnrPlot->setPlotTextColor(cv::Scalar( 35, 35, 35));
    lloydSnrPlot->render(lloydSnrPlotResult);
    cv::imshow("Lloyd's SNR(R)", lloydSnrPlotResult);

    cv::Mat lloydPsnrPlotResult;
    cv::Ptr<cv::plot::Plot2d> lloydPsnrPlot = cv::plot::Plot2d::create(lloydPsnrData);
    lloydPsnrPlot->setPlotBackgroundColor(cv::Scalar( 235, 235, 235 ));
    lloydPsnrPlot->setPlotLineColor(cv::Scalar( 50, 50, 255 ));
    lloydPsnrPlot->setInvertOrientation(true);
    lloydPsnrPlot->setPlotGridColor(cv::Scalar( 35, 35, 35));
    lloydPsnrPlot->setPlotTextColor(cv::Scalar( 35, 35, 35));
    lloydPsnrPlot->render(lloydPsnrPlotResult);
    cv::imshow("Lloyd's PSNR(R)", lloydPsnrPlotResult);

    cv::Mat lloydEntropyPlotResult;
    cv::Ptr<cv::plot::Plot2d> lloydEntropyPlot = cv::plot::Plot2d::create(lloydEntropyData);
    lloydEntropyPlot->setPlotBackgroundColor(cv::Scalar( 235, 235, 235 ));
    lloydEntropyPlot->setPlotLineColor(cv::Scalar( 50, 50, 255 ));
    lloydEntropyPlot->setInvertOrientation(true);
    lloydEntropyPlot->setPlotGridColor(cv::Scalar( 35, 35, 35));
    lloydEntropyPlot->setPlotTextColor(cv::Scalar( 35, 35, 35));
    lloydEntropyPlot->render(lloydEntropyPlotResult);
    cv::imshow("Lloyd's PSNR(H)", lloydEntropyPlotResult);

    std::vector<double> srcVec;
    std::vector<double> QVec;
    std::vector<int> values;
    int val = 0;
    for (int i = 0; i < quant.rows; i++)
    {
        for (int j = 0; j < quant.cols; j++)
        {
            srcVec.push_back(yDark.at<uchar>(i, j));
            QVec.push_back(quant.at<uchar>(i, j));
        }
    }

    cv::Mat QPlotResult;
    std::sort(QVec.begin(), QVec.end());
    std::sort(srcVec.begin(), srcVec.end());
    cv::Ptr<cv::plot::Plot2d> qPlot = cv::plot::Plot2d::create(QVec);
    qPlot->setPlotBackgroundColor(cv::Scalar( 235, 235, 235 ));
    qPlot->setPlotLineColor(cv::Scalar( 50, 50, 255 ));
    qPlot->setInvertOrientation(true);
    qPlot->setPlotGridColor(cv::Scalar( 35, 35, 35));
    qPlot->setPlotTextColor(cv::Scalar( 35, 35, 35));
    qPlot->render(QPlotResult);
    cv::imshow("Q(x)", QPlotResult);
}
