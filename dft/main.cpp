#include <iostream>
#include "dft.h"


int main() {

    std::vector<std::complex<double>> input;
    input.emplace_back(1.21, 0.2);
    input.emplace_back(2.0, 0.1);
    input.emplace_back(3.1, 1.1);
    input.emplace_back(0.4, 2.1);
    input.emplace_back(3.65, 0.21);
    input.emplace_back(1.73, 0.5);

    std::cout << "Input \n\n";
    for (auto num : input) {
        std::cout << num << std::endl;
    }
    std::cout << std::endl;

    auto output = dft::computeDFT(input);
    std::cout << "DFT \n\n";
    for (auto num : output) {
        std::cout << num << std::endl;
    }
    std::cout << std::endl;

    auto inverse_output = dft::computeDFT(output, true);
    std::cout << "Inverse DFT \n\n";
    for (auto num : inverse_output) {
        std::cout << num << std::endl;
    }
    std::cout << std::endl;

    auto matrix_output = dft::computeMatrixDFT(input);
    std::cout << "Matrix DFT \n\n";
    for (auto num : matrix_output) {
        std::cout << num << std::endl;
    }
    std::cout << std::endl;

    auto inverse_matrix_output = dft::computeMatrixDFT(matrix_output, true);
    std::cout << "Inverse Matrix DFT \n\n";
    for (auto num : inverse_matrix_output) {
        std::cout << num << std::endl;
    }
    std::cout << std::endl;

    //----------------------------------------------------------------//

    std::cout << "Linearity: au(k) + bu(k) <-> aU(n) + bU(n) \n\n";
    double alpha = 3.5;
    double beta = 0.4;
    std::vector<std::complex<double>> u1 = input, u2 = input;
    std::vector<std::complex<double>> left, right;
    std::vector<std::complex<double>> U1 = output, U2 = output;
    for (int i = 0; i < input.size(); i++) {
        u1[i] *= alpha;
        u2[i] *= beta;
        left.emplace_back(u1[i] + u2[i]);

        U1[i] *= alpha;
        U2[i] *= beta;
        right.emplace_back(U1[i] + U2[i]);
    }

    std::cout << "aU(n) + bU(n) \n\n";
    for (auto num : right) {
        std::cout << num << std::endl;
    }
    std::cout << std::endl;

    std::cout << "DTF from au(k) + bu(k) \n\n";
    auto linear_output = dft::computeDFT(left);
    for (auto num : linear_output) {
        std::cout << num << std::endl;
    }
    std::cout << std::endl;

    //----------------------------------------------------------------//
    std::cout << "Parseval's theorem: sum(u^2(k)) == 1/N * sum(abs(U^2(n)))\n\n";
    double time_energy = 0;
    for (auto num : input) {
        time_energy += abs(num * num);
    }
    std::cout << "Signal energy in time domain: " <<  time_energy << std::endl;

    double frequency_energy = 0;
    for (auto num : output) {
        frequency_energy += abs(num * num);
    }
    frequency_energy /= output.size();
    std::cout << "Signal energy in frequency domain: " <<  frequency_energy << std::endl;


    std::cout << "Calculation ended";
    return 0;
}